# build docker image NodeJS

1. Copy your source to "bin"

2. เปลี่ยน port dockerfile  
แก้ไขที่ EXPOSE ให้เป็น port ที่ต้องการ

3. Build
``` console
$ docker build -t <image name> .
```

4. Run
``` console
$ docker run -dit -p <host port>:<container port> -n <node name> <image name>
```

5. Done!!
